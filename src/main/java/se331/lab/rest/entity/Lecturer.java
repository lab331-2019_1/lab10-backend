package se331.lab.rest.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.*;

import javax.persistence.*;
import java.lang.ref.Reference;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Entity
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Lecturer {
    @Id
    String name;
    String surname;
    @OneToMany(mappedBy = "advisor")
    @Builder.Default
    List<Student> advisees = new ArrayList<>();
    @OneToMany(mappedBy = "lecturer")
    @JsonManagedReference
    //Set<Course> courses = new HashSet<>;
    @Builder.Default
    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    List<Course> courses = new ArrayList<>();
}