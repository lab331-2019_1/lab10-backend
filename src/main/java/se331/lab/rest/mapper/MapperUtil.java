package se331.lab.rest.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;
import se331.lab.rest.dto.LecturerDto;
import se331.lab.rest.entity.Lecturer;

import java.util.List;

@Mapper
public class MapperUtil {

    MapperUtil INSTANCE = Mappers.getMapper( MapperUtil.class);
    @Mappings({})
    LecturerDto getLecturerDto(Lecturer lecturer);
    @Mappings({})
    List<LecturerDto> getLecturerDto(List<Lecturer> lecturer);


}
