package se331.lab.rest.dto;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class LecturerDto {
    Long id;
    String name;
    String surname;


}
